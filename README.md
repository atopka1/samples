# Amy Topka #
Email: atopka1@gmail.com

Phone: (631) 901-2819

This is a collection of course work, UI samples, and other team/individual projects.

# TellMe
Senior design project for Hofstra University. 

Entered in Hofstra Digital Remedy Challenge 2017 (previously known as CPXi) and made to final round among 80+ candidates, also receiving an honorable mention.

My contribution: Created front end application, and developed web crawler.

* Front-End Source Code:  https://bitbucket.org/atopka1/tellme
* Heroku Demo (Functionality limited, not connected to back-end): https://hofstratellme.herokuapp.com/

Additional documentation and presentations are located under tellme folder.

# UI Design
Additional samples of UI Design for QBit Technologies, as well as independent projects, can be found under UI-Samples.

![picture](ui-samples/caribnews-1.png)

![picture](ui-samples/faers_home.png)

![picture](ui-samples/beacon_main.png)

![picture](ui-samples/beacon_nearby.png)

